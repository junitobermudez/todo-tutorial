# README #

First Tutorial using Node.js and Angular.js implementing the MEAN Full-Stack.

### What is this repository for? ###

* To-Do Web Application. Creates a list of to-dos and removing them when you are done.
* Learning Node.js, Angular.js, and the complete structure of Node.js server.
* Tutorial URL: https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)